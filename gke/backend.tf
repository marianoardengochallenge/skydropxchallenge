terraform {
 backend "gcs" {
   bucket  = "skydropx-challenge"
   prefix  = "terraform-gke/state"
 }
}
