terraform {
  backend "gcs" {
    bucket = "skydropx-challenge"
    prefix = "terraform-ce/state"
  }
}
