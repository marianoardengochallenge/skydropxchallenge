variable "app_name" {
  type = string
}
variable "app_environment" {
  type = string
}
variable "app_domain" {
  type = string
}
variable "app_project" {
  type = string
}
variable "app_node_count" {
  type = number
}
