# Application Definition 
app_name        = "skydropx" #do NOT enter any spaces
app_environment = "test"      # Dev, Test, Prod, etc
app_domain      = "test.com"
app_project     = "skydropx-336615"
app_node_count  = 2

# GCP Settings
gcp_region_1 = "europe-west1"
gcp_zone_1   = "europe-west1-b"
gcp_zone_2   = "europe-west1-c"

# GCP Netwok
private_subnet_cidr_1 = "10.10.1.0/24"
private_subnet_cidr_2 = "10.10.2.0/24"
